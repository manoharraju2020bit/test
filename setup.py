from setuptools import setup, find_packages

setup(
    name="gsk_cga_local",

    version="0.6.0dev9",
  
    packages=find_packages(),

    description="CGA project"
)
